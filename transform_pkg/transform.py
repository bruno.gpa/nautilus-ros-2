#!/usr/bin/env python3  

import rospy

import tf_conversions
import tf2_ros
import geometry_msgs.msg

import math


class SolarSystem:
    def __init__(self):
        self.br = tf2_ros.TransformBroadcaster()
        self.t = geometry_msgs.msg.TransformStamped()
    
    def run(self, planet):
        self.t.header.stamp = rospy.Time.now()    
        self.t.header.frame_id = 'Sol'

        self.t.child_frame_id = planet[0]
        self.t.transform.translation.x = planet[1] * math.cos(self.t.header.stamp.to_time() * planet[2])
        self.t.transform.translation.y = planet[1] * math.sin(self.t.header.stamp.to_time() * planet[2])
        self.t.transform.translation.z = 0.0

        self.t.transform.rotation.x = 0
        self.t.transform.rotation.y = 0
        self.t.transform.rotation.z = 0
        self.t.transform.rotation.w = 1

        self.br.sendTransform(self.t)


if __name__ == '__main__':
    try:
        rospy.init_node('orbits', anonymous=True)
        planets = ['mercurio', 'venus', 'terra', 'marte', 'jupiter', 'saturno', 'urano', 'netuno']

        s = SolarSystem()

        while not rospy.is_shutdown():
            for planet in planets:
                param = rospy.get_param('/%s/info' % planet)
                s.run(param)

    except rospy.ROSInterruptException:
        pass

